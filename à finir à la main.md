40 et 42 / 41, 42, 70



60 à 67 / 49-1, 49-2,... / 4 et 45
56 à 47


24 et 25

L. 10 du code de justice administrative et L. 111-13 du code de l’organisation judiciaire
article L. 211-2 du code du patrimoine / 15, 16, 18, 19, 20 et 21 du règlement (UE) 2016/679
article L. 6113-7 du code de la santé publique / article L. 6113-8 du même code / article L. 1462-1 du code de la santé publique / article L. 1462-1 du code de la santé publique / article 226-13 du code pénal / article L. 1111-2 du code de la santé / article L. 1121-1 du code de la santé publique, etc. même code
article L. 311-3-1 et du chapitre Ier du titre Ier du livre IV du code des relations du public et de l’administration

77 à 79 du règlement


28 à 31 / 31, 33 et 34 du règlement
article 38 de la Constitution


article 230-8 du code de procédure pénale / articles L. 114-1, L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l’article 17-1 de la loi n° 95-73 du 21 janvier 1995 d’orientation et de programmation relative à la sécurité / article 804 du même code
articles 21 et 22 de la présente loi / article 19 de la présente loi