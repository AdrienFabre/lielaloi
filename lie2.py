##### Ajout de quelques liens dans une loi #####

# Ce que fait ce script :
#   il trouve les occurrences des mots "article(s) x", où x est un nombre, et insère des liens markdown dans le texte
#   le fichier origine_articles détaille sur chaque ligne n les références de l'article n 
#   le k-ème caractère de la ligne n donne l'origine (loi 78, règlement UE...) de la k-ème occurrence des mots "article(s) x" dans l'article n
#   Précisions:
#    La regex cherche "article x" ou "articles x" où x est un nombre et la casse est respectée (notamment, "Article" (avec majuscule) et "article n'est" (sans nombre) ne doivent pas apparaître dans la regex)
#    l: loi 78 / r: règlement 2016/679 / o: ordonnance n° 2005-1516 du 8 décembre 2005 / d: directive (UE) 2016/680 / -: rien
#
# Ce que ce script ne fait pas #TODO:
#    - gérer les conflits avec les liens existants: ce script rajoute un lien quand il y en a déjà, ce qui risque de poser problème
#    - insérer des liens pour d'autres besoins que les articles de la loi de 78-17, du règlement 2016/679 ou de la diretive 2016-680:
#      en particulier, il faudrait rajouter des liens pour des mentions d'articles d'autres législations ainsi que pour les législations entières
#    - gérer les articles multiples: quand "articles 24 et 25" sont cités, seul un lien vers l'article 24 est fourni (du type [article 24](article/24))
#   les cas expliqués dans ces deux derniers points sont recensés dans le fichier "à finir à la main"

import os
import re

origin_directory = '../dossier-legislatif-protection-donnees-personnelles/pjl-490/'
destination_directory = '../dossier-legislatif-protection-donnees-personnelles/pjl-490 lie/'
origine_articles = open('./origine_articles', 'r').readlines()

def ajoute_liens(text, n):
	articles_n = re.findall(r'article(s?) ([0-9]+)', text)

	if len(origine_articles[n-1])-1 != len(articles_n):
		print("Wrong number of matches provided for article" + n)

	liens_article_n = []
	for k in range(1,len(origine_articles[n-1])):
		if origine_articles[n-1][k-1] == 'l':
			path_origin_nk = "/loi-78-17/articles/article-"
		if origine_articles[n-1][k-1] == 'r':
			path_origin_nk = "/reglement-2016-679/articles/article-"
		if origine_articles[n-1][k-1] == 'o':
			path_origin_nk = "" # TODO
		if origine_articles[n-1][k-1] == 'd':
			liens_article_n.append("/directive-2016-680/articles/article-")
		if origine_articles[n-1][k-1] == '-':
			path_origin_nk = "" # TODO (cf. fichier "à finir à la main")
		liens_article_n.append("[article" + articles_n[k-1][0] + " " + articles_n[k-1][1] + "](" + path_origin_nk + articles_n[k-1][1] + ")")

	article_n = re.sub(r'articles? [0-9]+', lambda m, i=iter(liens_article_n): next(i), text)

	return(article_n)

for titre in os.listdir(origin_directory):
	if not titre.endswith("md"):
		for chapitre in os.listdir(origin_directory + titre):
			path_origin = origin_directory + titre + "/" + chapitre
			path_destination = destination_directory + titre + "/" + chapitre
			if not chapitre.endswith("md"):
				for article in os.listdir(path_origin):
					n = int(re.search(r'[0-9]+', article).group(0)) # number of the article
					text = open(path_origin + '/' + article, 'r').read() # readlines ? (\n instead of new lines)
					text_lie = open(path_destination + '/' + article, 'w')
					
					text_modifie = ajoute_liens(text, n)

					text_lie.write(text_modifie)
					text_lie.close()

			if chapitre.startswith("article"):		
				n = int(re.search(r'[0-9]+', chapitre).group(0)) # number of the article
				text = open(path_origin, 'r').read()
				text_lie = open(path_destination, 'w')

				text_modifie = ajoute_liens(text, n)

				text_lie.write(text_modifie)
				text_lie.close()

