# LieLaLoi

## Crée des liens entre une loi et ses documents préparatoires

Ce logiciel génére un site web permettant de naviguer dans le projet de loi sur les données personnelles et les différents textes qui sont en rapport. Son objectif est de faciliter la compréhension du projet de loi et de pouvoir en débattre.

Ce projet est un [logiciel libre](LICENSE), développé par l'équipe de la députée [Paula Forteza](https://forteza.fr) dans le cadre du [bureau ouvert](https://forteza.fr/index.php/2017/10/26/bureau-ouvert/)

# Exemple d'utilisation

Pour un exemple d'utilisation, référez-vous au [Dossier législatif pour le projet de loi relatif à la protection des données personnelles](https://framagit.org/parlement-ouvert/dossier-legislatif-protection-donnees-personnelles)