#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Détecte les références entre les documents d'un dossier législatif et les remplace par des liens.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys


article_x_re = re.compile(r"(?ims)(?<=[ '’])article\s+(?P<numero>\d+)(((\.|\s+-\s+)(?P<i>\d+))|\s+\((?P<j>\d+)\))?")

documents_infos = [
    dict(
        path_template='/loi-78-17/article-{numero}',
        re_templates=[
            re.compile(r'(?ims)\s+de\s+(la\s+)?loi\s+((n°|no)\s*)?78-17'),
            re.compile(r'(?ims)\s+de\s+(la\s+)?loi\s+du\s+6\s+janvier\s+(19)?78'),
            ],
        ),
    dict(
        path_template='/reglement-2016-679/article-{numero}',
        re_templates=[
            re.compile(r'(?ims)\s+du\s+règlement\s+(\(UE\)\s+)?((n°|no)\s*)?2016/679'),
            ],
        ),
    dict(
        path_template='/directive-2016-680/article-{numero}',
        re_templates=[
            re.compile(r'(?ims)\s+de\s+(la\s+)?directive\s+((\(UE\))\s+)?((n°|no)\s*)?2016/660'),
            ],
        ),
    ]


def add_links(text):
    linked_text = text
    for article_match in article_x_re.finditer(text):
        next_index = article_match.end()
        numero_article = article_match.group('numero')
        replaced = False
        for document_infos in documents_infos:
            for re_template in document_infos['re_templates']:
                document_match = re_template.match(text[next_index:])
                if document_match is not None:
                    fragment = text[article_match.start():next_index + document_match.end()]
                    linked_text = linked_text.replace(
                        fragment,
                        '[{}]({})'.format(fragment, document_infos['path_template'].format(numero=numero_article)),
                        )
                    replaced = True
        # if not replaced:
        #     print('   ', article_match.group(0), numero_article, article_match.group('i'), text[next_index:next_index + 100])
    return linked_text


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'dir',
        help='path of directory containing Markdown text files',
        )
    args = parser.parse_args()

    for parent_dir, dir_names, filenames in os.walk(args.dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            file_path = os.path.join(parent_dir, filename)
            print('Adding links to {}'.format(file_path))
            with open(file_path, 'r', encoding='utf-8') as markdown_file:
                text = markdown_file.read()
            linked_text = add_links(text)
            if linked_text != text:
                print('    File has been modified.')
                with open(file_path, 'w', encoding='utf-8') as markdown_file:
                    markdown_file.write(linked_text)

    return 0


if __name__ == '__main__':
    sys.exit(main())
